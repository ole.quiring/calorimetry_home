import json
import os
import sys
import time
from functions import m_pck
from functions import m_json

from w1thermsensor import Sensor, W1ThermSensor
import h5py
import numpy as np


# def loadItems(data, filter_by, required_value):
#     values=[]
#     for i in data['setup']:
#         sensor = data['setup'][i][filter_by]
#         if sensor == required_value:
#             values.append(i)
#     return values
# 
path ="/home/pi/calorimetry_home/datasheets/setup_heat_capacity.json"
# f=open(path)
# data=json.load(f)
# 
# print(loadItems(data, 'type', 'sensor'))



# def get_metadata_from_setup(path: str) -> dict:
#     """Extracts IDs, names, and types from a setup JSON file.
# 
#     This function reads a JSON file with a specific structure and extracts IDs,
#     names, and types. The returned dictionary contains an 'all' key with lists
#     of all IDs and names, and keys for each unique type with lists of IDs and names
#     for each type. Refer to README.md section "Runtime metadata" for a detailed description
#     of the output data structure.
# 
#     Args:
#         path (str): The file path to the setup JSON.
# 
#     Returns:
#         dict: A dictionary with the metadata (IDs)
# 
#     """
#     # TODO: Complete the function.
#     f=open(path)
#     data=json.load(f)
#     
#     def loadUUID(data, filter_by, required_value):
#         values=[]
#         for i in data['setup']:
#             if filter_by == "":
#                 values.append(i)
#                 continue
#             sensor = data['setup'][i][filter_by]
#             if filter_by == "" or sensor == required_value:
#                 values.append(i)
#         return values
# 
# 
#     def loadItems(data, filter_by, required_value, received_value):
#         values=[]
#         for i in data['setup']:
#             if filter_by == "":
#                 values.append(data['setup'][i][received_value])
#                 continue
#             sensor = data['setup'][i][filter_by]
#             if filter_by == "" or sensor == required_value:
#                 values.append(data['setup'][i][received_value])
#         return values
# 
#     thisdict = {   
#         'all': {
#             
#             'values': loadUUID(data,"", "sensor"),
#             'names': loadItems(data, "", "", "name")
#         },
#         'setup_path': '/path/to/setup_up.json',
#         # Only for sensor type, there is serials key
#         'sensor': {
#             'values': loadUUID(data, "type", "sensor"),
#             'names': loadItems(data, "type", "sensor", "name"),
#             'serials': loadItems(data, "type", "sensor", "serial")
#         },
#         'group_info': {
#             'values': loadUUID(data, "type", "group_info"),
#             'names': loadItems(data, "type", "group_info", "name")
#         },
#         'instrument': {
#             'values': loadUUID(data, "type", "instrument"),
#             'names': loadItems(data, "type", "instrument", "name")
#         },
#         'probe': {
#             'values': loadUUID(data, "type", "probe"),
#             'names': loadItems(data, "type", "probe", "name")
#         },
#     }
#     return thisdict
# print(get_metadata_from_setup(path))
for sensor in W1ThermSensor.get_available_sensors():
    print("The sensor with the serial %s has the temperature %.2f" % (sensor.id, sensor.get_temperature()))


path ="/home/pi/calorimetry_home/datasheets/setup_heat_capacity.json"
#metadata = m_jsonget_metadata_from_setup(path)


# data = {i: [[], []] for i in metadata["sensor"]["values"]}
# start = time.time()
# sensor_list = [
#     W1ThermSensor(Sensor.DS18B20, id) for id in metadata["sensor"]["serials"]
# ]
# 
# for i, sensor in enumerate(sensor_list):
#     for y, current_id in enumerate(data):
#         if i == y:
#             temp_stamp_data = []
#             current_time= time.time()
#             current_temperature=sensor.get_temperature()
#             temp_stamp_data.append([current_temperature, current_time])
#             data[current_id] = temp_stamp_data
#         
# meta=m_json.get_metadata_from_setup(path)
# 
# print(meta)
# #amogsu = m_pck.get_meas_data_calorimetry(get_metadata_from_setup(path))
# sus = m_pck.logging_calorimetry(m_pck.get_meas_data_calorimetry(meta),meta,"/home/pi/calorimetry_home/h5stuff","/home/pi/calorimetry_home/datasheets")
# print(sus)
#print(amogsu)
# for sensor, uuid in zip(sensor_list, data):
#     print(f"{sensor.get_temperature()} is {uuid}")
# print(list)
#print(data)

# with open('/home/pi/calorimetry_home/output.json', 'w') as outfile:
#     outfile.write(json.dumps(get_metadata_from_setup(path), indent=4))

# 
# pathh5="h5stuff/h5stuff.h5"
# ###
# import h5py
# 
# dataset = h5py.File(pathh5, "r+")
# ####
# temp1 = []
# time1 = []
# temp2 = []
# time2 = []
# for k, current in enumerate(dataset['RawData']):
#     for content in dataset["RawData"][current]:
#         for i, actual_contents in enumerate(dataset['RawData'][current][content]):
#             if k == 0:
#                 if content == 'time_stamp':
#                     time1.append(actual_contents[0])
#                     continue
#                 temp1.append(actual_contents[0])
#             elif k == 1:
#                 if content == 'time_stamp':
#                     time2.append(actual_contents[0])
#                     continue
#                 temp2.append(actual_contents[0])
# matplotlib.plt.scatter(time1,temp1)
#
list = []
for i in range(20):
    list.append(time.time_ns())
print(list)




