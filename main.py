from functions import m_json
from functions import m_pck

path = "/home/pi/calorimetry_home/datasheets/setup_newton.json"
datasheetpath ="/home/pi/calorimetry_home/datasheets"
metadata = m_json.get_metadata_from_setup(path)
A=m_pck.check_sensors()
archive_path = "/home/pi/calorimetry_home/h5stuff2"
#for i in A:
m_pck.logging_calorimetry(m_pck.get_meas_data_calorimetry(metadata),metadata,archive_path,datasheetpath)

m_json.archiv_json(datasheetpath,path,archive_path)
    